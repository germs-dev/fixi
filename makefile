TIMEOUT ?= 5
all: units

/tmp/makefile:
	curl --silent -L turpin.cloud/makefile --output $@

units: /tmp/makefile
	@$(MAKE) EXTRA=-DNDEBUG --no-print-directory -j $(shell nproc) --directory=fixer --makefile=$<
	@$(MAKE) EXTRA=-DNDEBUG --no-print-directory -j $(shell nproc) --directory=fixee --makefile=$<
	@$(MAKE) EXTRA=-DNDEBUG --no-print-directory -j $(shell nproc) --directory=test --makefile=$<

run: units
	# Test writing to null
	timeout $(TIMEOUT) fixer/app.o > /dev/null || true
	# Test piping to fix processor
	timeout $(TIMEOUT) fixer/app.o | fixee/app.o || true

test: /tmp/makefile units
	$(MAKE) EXTRA=-DNDEBUG run --directory=test --makefile=$<

libs: /tmp/makefile
	$(MAKE) libs --makefile=$<

clean: /tmp/makefile
	$(MAKE) clean --directory=fixer --makefile=$<
	$(MAKE) clean --directory=fixee --makefile=$<
	$(MAKE) clean --directory=test --makefile=$<
	$(RM) /tmp/makefile

