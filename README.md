---
title: FIXUNE -- 狐
author: Dean Turpin
---

Exploring FIX processing using the latest C++.

## LinkedIn challenge

A FIX coding challenge posted on the C++ group on LinkedIn -- see [the post](https://www.linkedin.com/feed/update/urn:li:activity:7046213916720295936/).

> Given that HFT firmware can make trading decisions in 10ns -- see [Trading at light speed: designing low latency systems in C++](https://youtu.be/8uAW5FQtcvE) for more info -- I was curious how quickly I could process a FIX message in software.
> The process is timed using Google Benchmark; and just for kicks I've written my solution constexpr as I don't get to use it very often in my day job.
> The challenge: extract the ticker symbol name and order quantity from a ASCII FIX message as quickly as possible.
>
> The tags to search for: 55=Symbol 38=OrderQty

### Results

On a Sapphire Rapids VM I got my ranges solution below 100ns -- not bad... --- but Oleg Morozov managed to get his AVX version under 4ns!

```
  Model name:            Intel(R) Xeon(R) Platinum 8481C CPU @ 2.70GHz
    BogoMIPS:            5399.99
```

```
---------------------------------------------------------------------------
Benchmark                                 Time             CPU   Iterations
---------------------------------------------------------------------------
challenge_fix_dean_turpin              89.3 ns         89.3 ns      7850558
challenge_fix_oleg_morozov             3.72 ns         3.72 ns    189516479
```

## References
- [SIMD-friendly algorithms for substring searching](http://0x80.pl/articles/simd-strfind.html)
- [Wikipedia](https://en.wikipedia.org/wiki/Financial_Information_eXchange)
- [What is FIX protocol?](https://targetcompid.tumblr.com/tagged/fix%20protocol%20fix%20parser%20targetcompid)
- [Online FIX parser](https://fixparser.chronicle.software)
- [quickFIX engine](https://quickfixengine.org/)
- [FIX trading community](https://www.fixtrading.org/what-is-fix/).

