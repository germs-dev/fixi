#include "benchmark/benchmark.h"
#include "gtest/gtest.h"
#include <cassert>
#include <ranges>
#include <string>
#include <string_view>

namespace {

// sz suffix on string literals
using namespace std::string_view_literals;

// Example FIX message
constexpr auto fix1 = +"8=FIX.4.2|9=69|35=D|49=Partner|56=Client|34=3|52="
                       "20200822-09:00:00.000|11=ORDER0012345|21=1|55=MSFT|"
                       "54=2|38=100|40=1|59=0|60=20200822-09:00:00.000|"
                       "10=152|";

// Extract key/value pair from a string
constexpr std::pair<std::string_view, std::string_view>
get_key_value(const std::string_view token) {

  // Split on equals
  const size_t pos = token.find('=');
  const std::string_view key = token.substr(0, pos);
  const std::string_view value = token.substr(pos + 1);

  // Return pair of results
  return {key, value};
}

static_assert(get_key_value("") == std::make_pair(""sv, ""sv));
static_assert(get_key_value("8=FIX.4.2") == std::make_pair("8"sv, "FIX.4.2"sv));

// Tokenise a FIX message and return summary
constexpr std::pair<size_t, std::string_view>
tokenise(const std::string_view message) {

  // Initialise field count and ticker symbol
  auto field_count = 0uz;    // uz is size_t in C++23
  auto ticker_symbol = ""sv; // string_view literal

  // Split message into key/value pairs using tag delimiter
  for (const auto token : std::views::split(message, '|')) {

    // Split token
    const auto &[key, value] = get_key_value(std::string_view{token});

    // Store the value if it's the key we're looking for
    if (key == "55"sv)
      ticker_symbol = value;

    ++field_count;
  }

  // Report number of tags and the company name
  return {field_count, ticker_symbol};
}

static_assert(tokenise("") == std::make_pair(0uz, ""sv));
static_assert(tokenise(fix1) == std::make_pair(17uz, "MSFT"sv));

// Get a single tag from a message
constexpr std::string_view get_tag(const std::string_view message,
                                   const std::string_view tag) {

  // Split message into key/value pairs using tag delimiter
  for (const auto token : std::views::split(message, '|')) {

    // Split token
    const auto &[key, value] = get_key_value(std::string_view{token});

    // Store the value if it's the key we're looking for
    if (key == tag)
      return value;
  }

  return {};
}
static_assert(get_tag("", "55"sv) == ""sv);
static_assert(get_tag(fix1, "ABC"sv) == ""sv);
static_assert(get_tag(fix1, "11"sv) == "ORDER0012345"sv);

// Test
TEST(fix, tokenise_hashmap) {
  EXPECT_EQ(tokenise(""), std::make_pair(0uz, ""sv));
  EXPECT_EQ(tokenise(fix1), std::make_pair(17uz, "MSFT"sv));
  EXPECT_EQ(get_tag("", "55"sv), ""sv);
  EXPECT_EQ(get_tag(fix1, "40"sv), "1"sv);
}

void fix_get_key_value3(benchmark::State &state) {
  for (auto _ : state)
    assert(get_key_value("8=FIX.4.2") == std::make_pair("8"sv, "FIX.4.2"sv));
}

void fix_get_summary_hashmap(benchmark::State &state) {
  for (auto _ : state)
    assert(tokenise(fix1) == std::make_pair(17uz, "MSFT"sv));
}

void fix_get_tag_first(benchmark::State &state) {
  for (auto _ : state)
    assert(get_tag(fix1, "8"sv) == "FIX.4.2"sv);
}

void fix_get_tag_last(benchmark::State &state) {
  for (auto _ : state)
    assert(get_tag(fix1, "10"sv) == "152"sv);
}

BENCHMARK(fix_get_key_value3);
BENCHMARK(fix_get_summary_hashmap);
BENCHMARK(fix_get_tag_first);
BENCHMARK(fix_get_tag_last);
} // namespace
