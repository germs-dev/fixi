#include "benchmark/benchmark.h"
#include "gtest/gtest.h"
#include <cassert>
#include <ranges>
#include <string>
#include <string_view>

/*
Given that HFT firmware can make trading decisions in 10ns -- see this excellent
talk for more info: https://youtu.be/8uAW5FQtcvE -- I was curious how quickly I
could process a FIX message in software.

The process is timed using Google Benchmark; and just for kicks I've written my
solution constexpr as I don't get to use it very often in my day job.

The challenge: extract the ticker symbol name and order quantity from a ASCII
FIX message as quickly as computerly possible.

The tags to search for:
55=Symbol
38=OrderQty
*/

namespace {
// Example FIX message, note the unary plus operator
constexpr auto fix1 = +"8=FIX.4.2|9=69|35=D|49=Partner|56=Client|34=3|52="
                       "20200822-09:00:00.000|11=ORDER0012345|21=1|55=MSFT|"
                       "54=2|38=100|40=1|59=0|60=20200822-09:00:00.000|"
                       "10=152|";

// Enable "sv" suffix on string literals
using namespace std::string_view_literals;

// Return the value component of a key/value pair
constexpr std::string_view get_value(const std::string_view token) {
  return token.substr(token.find_first_of('=') + 1);
};

static_assert(get_value("").empty());
static_assert(get_value("11=ORDER0012345") == "ORDER0012345");

// Parse a FIX message and return the ticker symbol and order quantity
constexpr std::pair<std::string_view, std::string_view>
get_order(const std::string_view message) {

  // Initialise field count and ticker symbol
  auto ticker_symbol = ""sv;
  auto order_quantity = ""sv;

  // Split message into tokens based on tag delimiter
  for (const auto key_value : std::views::split(message, '|')) {

    const auto token = std::string_view{key_value};

    // Store the value of the quantity tag
    if (order_quantity.empty())
      if (token.starts_with("38="))
        order_quantity = get_value(token);

    // Store the value of the symbol tag
    if (ticker_symbol.empty())
      if (token.starts_with("55="))
        ticker_symbol = get_value(token);

    // Check if we are done
    if (not order_quantity.empty() and not ticker_symbol.empty())
      return {ticker_symbol, order_quantity};
  }

  return {};
}

static_assert(get_order("") == std::make_pair(""sv, ""sv));
static_assert(get_order(fix1) == std::make_pair("MSFT"sv, "100"sv));

TEST(fix, get_order) {
  EXPECT_EQ(get_value(""), "");
  EXPECT_EQ(get_value("11=ORDER0012345"), "ORDER0012345");
  EXPECT_EQ(get_order(""), std::make_pair(""sv, ""sv));
  EXPECT_EQ(get_order(fix1), std::make_pair("MSFT"sv, "100"sv));
}

void challenge_fix_dean_turpin(benchmark::State &state) {
  for (auto _ : state)
    assert(get_order(fix1) == std::make_pair("MSFT"sv, "100"sv));
}

BENCHMARK(challenge_fix_dean_turpin);

} // namespace
