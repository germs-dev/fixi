#include "benchmark/benchmark.h"
#include <chrono>
#include <fmt/core.h>
#include <fmt/ranges.h>
#include <sstream>
#include <string>
#include <vector>

int run_test_suite(int, char **);

namespace devel {
std::string file_read(const std::string_view);
} // namespace devel

namespace devel2 {
std::string file_read(const std::string);
} // namespace devel

namespace fix {
std::string file_read(const std::string);
std::vector<std::pair<std::string, std::string>> tokenise(const std::string);
} // namespace fix

using fix::tokenise;

////////////////////////////////////////////////////////////////////////////////

constexpr auto fix_file_large = +"fix3.txt";

// Process FIX messages and print summary
void process() {

  // Read file
  const auto messages = fix::file_read(fix_file_large);
  std::stringstream ss;
  ss << messages;

  // Extract tags and report
  std::string message;
  while (std::getline(ss, message))
    const auto tags = tokenise(message);
}

void process_devel() {

  // Read file
  const auto messages = devel::file_read(fix_file_large);
  std::stringstream ss;
  ss << messages;

  // Extract tags and report
  std::string message;
  while (std::getline(ss, message))
    const auto tags = tokenise(message);
}

void process_devel2() {

  // Read file
  const auto messages = devel2::file_read(fix_file_large);
  std::stringstream ss;
  ss << messages;

  // Extract tags and report
  std::string message;
  while (std::getline(ss, message))
    const auto tags = tokenise(message);
}

// Entry point
int main(int argc, char **argv) {

  // Run main routine
  const auto start = std::chrono::high_resolution_clock::now();
  process();
  const auto end = std::chrono::high_resolution_clock::now();

  // Calculate the elapsed time
  const auto elapsed_time =
      std::chrono::duration_cast<std::chrono::nanoseconds>(end -
                                                           start)
          .count();

  // Output the elapsed time
  fmt::print("Processing took {}ns\n", elapsed_time);

  // Run Google Tests and Benchmarks
  return run_test_suite(argc, argv);
}

void main_process(benchmark::State &state) {
  for (auto _ : state)
    process();
}

void main_process_devel(benchmark::State &state) {
  for (auto _ : state)
    process_devel();
}

void main_process_devel2(benchmark::State &state) {
  for (auto _ : state)
    process_devel2();
}

BENCHMARK(main_process);
BENCHMARK(main_process_devel);
BENCHMARK(main_process_devel2);
