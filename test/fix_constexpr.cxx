#include "benchmark/benchmark.h"
#include "gtest/gtest.h"
#include <cassert>
#include <ranges>
#include <string>
#include <string_view>

namespace {

// sz suffix on string literals
using namespace std::string_view_literals;

// Example FIX message
constexpr auto fix1 = +"8=FIX.4.2|9=69|35=D|49=Partner|56=Client|34=3|52="
                       "20200822-09:00:00.000|11=ORDER0012345|21=1|55=MSFT|"
                       "54=2|38=100|40=1|59=0|60=20200822-09:00:00.000|"
                       "10=152|";

// Extract key/value pair from a string
constexpr std::pair<std::string_view, std::string_view>
get_key_value(const std::string_view token) {

  // Split on equals
  const size_t pos = token.find('=');
  const std::string_view key = token.substr(0, pos);
  const std::string_view value = token.substr(pos + 1);

  // Return pair of results
  return {key, value};
}

static_assert(get_key_value("") == std::make_pair(""sv, ""sv));
static_assert(get_key_value("8=FIX.4.2") == std::make_pair("8"sv, "FIX.4.2"sv));

// Tokenise a FIX message and return summary
// The summary is a pair of total fields and the ticker symbol (tag 55)
constexpr std::pair<size_t, std::string_view>
get_summary(const std::string_view message) {

  // Initialise field count and ticker symbol
  auto field_count = 0uz;    // uz is size_t in C++23
  auto ticker_symbol = ""sv; // string_view literal

  // Split message into key/value pairs using tag delimiter
  for (const auto token : std::views::split(message, '|')) {

    // Split token
    const auto &[key, value] = get_key_value(std::string_view{token});

    // Store the value if it's the key we're looking for
    if (key == "55"sv)
      ticker_symbol = value;

    ++field_count;
  }

  // Report number of tags and the company name
  return {field_count, ticker_symbol};
}

static_assert(get_summary("") == std::make_pair(0uz, ""sv));
static_assert(get_summary(fix1) == std::make_pair(17uz, "MSFT"sv));

// Test
TEST(fix, get_summary) {
  EXPECT_EQ(get_summary(""), std::make_pair(0uz, ""sv));
  EXPECT_EQ(get_summary(fix1), std::make_pair(17uz, "MSFT"sv));
}

void fix_get_key_value(benchmark::State &state) {
  for (auto _ : state)
    assert(get_key_value("8=FIX.4.2") == std::make_pair("8"sv, "FIX.4.2"sv));
}

BENCHMARK(fix_get_key_value);

void fix_get_summary_constexpr(benchmark::State &state) {
  for (auto _ : state)
    assert(get_summary(fix1) == std::make_pair(17uz, "MSFT"sv));
}

BENCHMARK(fix_get_summary_constexpr);
} // namespace
