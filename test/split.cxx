#include "benchmark/benchmark.h"
#include "gtest/gtest.h"
#include <utility>
#include <vector>

namespace fix {

// Split key-value pair string and return individual parts
std::pair<std::string, std::string> split_key_value(const std::string kv) {

  // Get key-value delimiter position
  const size_t pos = kv.find('=');
  const std::string key = kv.substr(0, pos);
  const std::string value = kv.substr(pos + 1);

  return {key, value};
}

// Test
TEST(parse, split_key_values) {
  using kv_t = std::pair<std::string, std::string>;
  EXPECT_EQ(split_key_value(""), kv_t("", ""));
  EXPECT_EQ(split_key_value("a=b"), kv_t("a", "b"));
  EXPECT_EQ(split_key_value("8=FIX.4.2"), kv_t("8", "FIX.4.2"));
}

void split_key_value_pair_with_substr(benchmark::State &state) {
  constexpr auto kv = +"8=FIX.4.2";
  for (auto _ : state)
    split_key_value(kv);
}
BENCHMARK(split_key_value_pair_with_substr);
} // namespace fix
