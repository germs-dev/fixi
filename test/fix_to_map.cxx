#include "benchmark/benchmark.h"
#include "gtest/gtest.h"
#include <map>
#include <ranges>
#include <string>
#include <vector>

// Proto
namespace fix {
std::pair<std::string, std::string> split_key_value(const std::string);
}

namespace {

// Parse message string and return a map of key/value pairs
std::map<std::string, std::string> tokenise(const std::string message) {

  std::map<std::string, std::string> tags;

  // Split message on a delimiter
  for (const auto tag_value : std::views::split(message, '|')) {
    const auto kv = std::string{std::cbegin(tag_value), std::cend(tag_value)};
    tags.emplace(fix::split_key_value(kv));
  }

  return tags;
}

// An example FIX message
constexpr auto message =
    +"8=FIX.4.2|9=69|35=D|49=Partner|56=Client|34=3|52=20200822-09:00:00.000|"
     "11="
     "ORDER0012345|21=1|55=MSFT|54=2|38=100|40=1|59=0|60=20200822-09:00:00.000|"
     "10=152|";

TEST(fix, tokenise_to_map) {
  const auto tags = tokenise(message);
  EXPECT_EQ(tags.size(), 17);
}

void fix_tokenise_to_map(benchmark::State &state) {
  for (auto _ : state)
    tokenise(message);
}
BENCHMARK(fix_tokenise_to_map);
} // namespace
