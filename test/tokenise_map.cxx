#include "benchmark/benchmark.h"
#include "gtest/gtest.h"
#include <cassert>
#include <ranges>
#include <string>
#include <string_view>

namespace fix {
std::pair<std::string, std::string> split_key_value(const std::string);
std::vector<std::pair<std::string, std::string>> tokenise(const std::string);
}

namespace {

// sz suffix on string literals
using namespace std::string_view_literals;

// Example FIX message
constexpr auto fix1 = +"8=FIX.4.2|9=69|35=D|49=Partner|56=Client|34=3|52="
                       "20200822-09:00:00.000|11=ORDER0012345|21=1|55=MSFT|"
                       "54=2|38=100|40=1|59=0|60=20200822-09:00:00.000|"
                       "10=152|";


// Tokenise a FIX message and return summary
auto
tokenise(const std::string_view ) {

  // // Initialise field count and ticker symbol
  // auto field_count = 0uz;    // uz is size_t in C++23
  // auto ticker_symbol = ""sv; // string_view literal

  // // Split message into key/value pairs using tag delimiter
  // for (const auto token : std::views::split(message, '|')) {

  //   // Split token
  //   const auto &[key, value] = get_key_value(std::string_view{token});

  //   // Store the value if it's the key we're looking for
  //   if (key == "55"sv)
  //     ticker_symbol = value;

  //   ++field_count;
  // }

  // // Report number of tags and the company name
  // return {field_count, ticker_symbol};

  return std::map<std::string, std::string>{};
}

TEST(fix, tokenise_map) {
  EXPECT_TRUE(tokenise("").empty());
  EXPECT_EQ(fix::tokenise(fix1).size(), 17uz);
  // EXPECT_EQ(tokenise(fix1), std::make_pair(17uz, "MSFT"sv));
  // EXPECT_EQ(get_tag("", "55"sv), ""sv);
  // EXPECT_EQ(get_tag(fix1, "40"sv), "1"sv);
}

// void fix_tokenise_map(benchmark::State &state) {
//   for (auto _ : state)
//     assert(tokenise(fix1) == std::make_pair(17uz, "MSFT"sv));
// }

// BENCHMARK(fix_get_key_value4);
// BENCHMARK(fix_tokenise_map);
} // namespace
