#include "benchmark/benchmark.h"
#include "gtest/gtest.h"
#include <fstream>
#include <string>

namespace devel {

// Split key/value pair string and return individual parts
std::string file_read(const std::string_view file_name) {

  std::ifstream in{std::string{file_name}};

  // Run to the end
  in.seekg(0, std::ios::end);

  // Reserve enough storage for the file contents
  std::string out(in.tellg(), '\0');

  // Back to the start
  in.seekg(0, std::ios::beg);

  // Copy the file contents
  in.read(out.data(), out.size());

  return out;
}

// Test
constexpr auto fix_file_small = +"fix1.txt";
constexpr auto fix_file_medium = +"fix2.txt";
constexpr auto fix_file_large = +"fix3.txt";

TEST(file, read) {
  // EXPECT_EQ(file_read(""), "");
  // EXPECT_EQ(file_read("/"), "");
  // EXPECT_EQ(file_read("\n"), "");
  // EXPECT_EQ(file_read("\1"), "");
  // EXPECT_EQ(file_read("notafile"), "");
  EXPECT_FALSE(file_read(fix_file_small).empty());
  EXPECT_TRUE(file_read(fix_file_small).contains("FIX"));
}

void read_tellg_small(benchmark::State &state) {
  for (auto _ : state)
    const auto messages = file_read(fix_file_small);
}

void read_tellg_medium(benchmark::State &state) {
  for (auto _ : state)
    const auto messages = file_read(fix_file_medium);
}

void read_tellg_large(benchmark::State &state) {
  for (auto _ : state)
    const auto messages = file_read(fix_file_large);
}

BENCHMARK(read_tellg_small);
BENCHMARK(read_tellg_medium);
BENCHMARK(read_tellg_large);
} // namespace devel
