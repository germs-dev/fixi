#include "benchmark/benchmark.h"
#include "gtest/gtest.h"
#include <fstream>
#include <string>

namespace devel2 {

// Split key/value pair string and return individual parts
std::string file_read(const std::string file_name) {

  std::ifstream in{file_name};
  const std::string out(std::istreambuf_iterator<char>(in), {});

  return out;
}

// Test
constexpr auto fix_file_small = +"fix1.txt";
constexpr auto fix_file_medium = +"fix2.txt";
constexpr auto fix_file_large = +"fix3.txt";

TEST(file, read) {
  EXPECT_EQ(file_read(""), "");
  // EXPECT_EQ(file_read("/"), "");
  EXPECT_EQ(file_read("\n"), "");
  EXPECT_EQ(file_read("\1"), "");
  EXPECT_EQ(file_read("notafile"), "");
  EXPECT_FALSE(file_read(fix_file_small).empty());
  EXPECT_TRUE(file_read(fix_file_small).contains("FIX"));
}

void read_streambuf_iterator_small(benchmark::State &state) {
  for (auto _ : state)
    const auto messages = file_read(fix_file_small);
}

void read_streambuf_iterator_medium(benchmark::State &state) {
  for (auto _ : state)
    const auto messages = file_read(fix_file_medium);
}

void read_streambuf_iterator_large(benchmark::State &state) {
  for (auto _ : state)
    const auto messages = file_read(fix_file_large);
}

BENCHMARK(read_streambuf_iterator_small);
BENCHMARK(read_streambuf_iterator_medium);
BENCHMARK(read_streambuf_iterator_large);
} // namespace
