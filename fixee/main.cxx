#include "fmt/core.h"
#include <atomic>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

int run_test_suite(int, char **);
namespace fix {
std::vector<std::pair<std::string, std::string>> tokenise(const std::string);
}

int main(int argc, char **argv) {

  auto bytes_processed = std::atomic{0uz};

  // Create reporter thread
  std::jthread t{[&](std::stop_token stop_token) {
    fmt::print(stderr, "FIXUNE - fixee\n");
    static auto bytes_written_so_far{0uz};

    using namespace std::literals::chrono_literals;
    // Report until out of scope
    while (not stop_token.stop_requested()) {
      std::this_thread::sleep_for(1s);

      const auto current_bytes_written = bytes_processed.load();
      const double gbps =
          8.0 *
          static_cast<double>(current_bytes_written - bytes_written_so_far) /
          10e9;

      fmt::print(stderr, "IN {}\t| Gb/s\n", gbps);
      bytes_written_so_far = current_bytes_written;
    }
  }};

  std::string message;
  while (std::cin >> message) {
    bytes_processed += message.size();
    const auto tags = fix::tokenise(message);
  }

  std::cout << "processed bits " << bytes_processed * 8 << "\n";

  // Run Google Tests and Benchmarks
  return run_test_suite(argc, argv);
}
