## Example messages
> Body length is the character count starting at tag 35 (included) all the way to tag 10 (excluded). SOH delimiters do count in body length.

> 8=FIX.4.2 | 9=178 | 35=8 | 49=PHLX | 56=PERS | 52=20071123-05:30:00.000 | 11=ATOMNOCCC9990900 | 20=3 | 150=E | 39=E | 55=MSFT | 167=CS | 54=1 | 38=15 | 40=2 | 44=15 | 58=PHLX EQUITY TESTING | 59=0 | 47=C | 32=0 | 31=0 | 151=15 | 14=0 | 6=0 | 10=128 | 

`SOH` character is used as a delimiter.

- [NYSE](https://www.nyse.com/publicdocs/nyse/markets/nyse/NYSE_CCG_FIX_Sample_Messages.pdf)
- [FIXSIM](https://www.fixsim.com/sample-fix-messages)

```
8=FIX.4.49=14835=D34=108049=TESTBUY152=20180920-18:14:19.50856=TESTSELL111=63673064027889863415=USD21=238=700040=154=155=MSFT60=20180920-18:14:19.49210=092 
```

## FIX protocol (explained by a quant developer)
See [Coding Jesus](https://www.youtube.com/watch?v=uZ8UEVhtPAo) and [CME FIX
standard](https://www.cmegroup.com/confluence/display/EPICSANDBOX/iLink+2+Standard+Header+-+Client+System+to+CME+Globex)
for the tag specification.

There are two layers:
- Session (log on, heartbeat, data integrity, sequencing)
- Application (buy, sell, cancel, modify, replace, quote)

### Refs
- https://www.onixs.biz/fix-dictionary/4.4/fields_by_tag.html
- https://www.fixglobal.com/trader-fix-tags-reading/

## Must have
See [Darwinex](https://www.youtube.com/watch?v=wSgAwJyev2Y).

### Standard header
| tag| value |
|----|----|
| 8  | BeginString |
| 9  | BodyLength  | 
| 35 | MsgType  |
| 49 | SenderCompID  |
| 56 | TargetCompID  |
| 34 | MsgSeqNum  |
| 52 | SendingTime  |

### Message Body
Sequence of `tab=value` pairs depending on message type.

### Standard trailer
- 10 CheckSum

## Example tags
### Header tags 8-255
- 35=c (message type)

### Body tags 256+
- 1028=Y (manual)
- 1029=N (auto)
- 321=1 (new instrument)
- 762=COMBO (type of instrument)

### Trailer tags
- 10=241 (checksum)

## Examples order
See [part 1](https://www.youtube.com/watch?v=rW8sDkHBfQ4) and [Darwinex tutorial](https://www.youtube.com/watch?v=pell_OwOmMw).

[stunnel](https://www.stunnel.org/) used for order session authentication, price sessions don't require SSH.

### Message type
- 35=D -- new order single
- 35=0 -- heartbeat
- 35=1 -- test request
- 35=8 -- execution report
- 35= -- session reject

### Symbol
- 55=IBM
- 55=EUR/USD

### Order ack
- 39=0 -- new order ack
- 39=8 -- new order reject
- 39=1 -- partial execution
- 39=2 -- complete execution

### Side
- 54=1 -- buy side
- 54=2 -- sell side

### Price
- 44 -- what is your limit on the order?

### Order type
- 40=1 -- markget price
- 40=2 -- limit price

### Routing
- 49=tbd -- sender comp id (sender firm)
- 50=tbd -- sender sub id (trader of desk ID)
- 56=tbd -- target comp id (sending order to)

## APIs
- https://exchangeratesapi.io/

