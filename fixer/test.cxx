#include "benchmark/benchmark.h"
#include "gtest/gtest.h"

// Entry point for both Google Test and Benchmark
int run_test_suite(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  const auto success = RUN_ALL_TESTS();

  benchmark::Initialize(&argc, argv);
  benchmark::RunSpecifiedBenchmarks();
  benchmark::Shutdown();

  return success;
}
