#include "fmt/core.h"
#include <atomic>
#include <iostream>
#include <string_view>
#include <thread>

constexpr std::string_view message{
    "8=FIX.4.2|9=65|35=D|49=BUYER|56=SELLER|34=1|52=20200612-10:15:32|11="
    "ORDER123|21=1|38=1000|40=2|54=1|55=IBM|60=20200612-10:15:32|10=252|"};

int run_test_suite(int, char **);

int main(int argc, char **argv) {

  // Total byte written
  auto bytes_written = std::atomic{0uz};

  using namespace std::literals::chrono_literals;

  // Create reporter thread
  std::jthread t{[&](std::stop_token stop_token) {
    fmt::print(stderr, "FIXUNE - fixer\n");
    static auto bytes_written_so_far{0uz};

    // Report until out of scope
    while (not stop_token.stop_requested()) {
      std::this_thread::sleep_for(1s);

      const auto current_bytes_written = bytes_written.load();
      const double gbps =
          8.0 *
          static_cast<double>(current_bytes_written - bytes_written_so_far) /
          10e9;
      fmt::print(stderr, "OUT {}\t| Gb/s\n", gbps);
      bytes_written_so_far = current_bytes_written;
    }
  }};

  // Main loop
  for (size_t i{}; i++ < 1'000'000'00;) {

    fmt::print("{}\n", message);

    bytes_written += message.size();
  }

  // Run Google Tests and Benchmarks
  return run_test_suite(argc, argv);
}
